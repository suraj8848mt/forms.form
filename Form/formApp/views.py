from django.shortcuts import render
from django.shortcuts import render, redirect 
from django.contrib.auth import authenticate, login 
from .forms import LoginForm



def login_page(request): 
	form = LoginForm(request.POST or None) 
	context = { "form": form } 
	if form.is_valid(): 
		username = form.cleaned_data.get('username') 
		password = form.cleaned_data.get('password') 
		user = authenticate(request, username=username, password=password) 
		if user is not None: 
			login(request, user) 
			return redirect("/login") 
		else: print('error') 
	return render(request, "index.html", context)
